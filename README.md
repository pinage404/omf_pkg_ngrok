<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>
# omf_pkg_ngrok

> A plugin for [Oh My Fish][omf] and [ngrok][ngrok].

[![Fish Shell Version][fish_version]][fish]
[![Oh My Fish Framework][omf_image]][omf]
[![Licence MIT][licence_mit_image]][mit]

<br/>

**This plugin provides many autocompletions for [`ngrok`][ngrok]**

-------------------------------------------------------------------------------

## Requirements

[`ngrok`][ngrok] must be available in the `$PATH`


## Install

```fish
$ omf install ngrok
```


## Usage

```fish
$ ngrok # then pess [TAB] to see autocompletion
```


## License

[MIT][mit] © [pinage404][author] et [al][contributors]


[fish]:              https://fishshell.com
[fish_version]:      https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square

[omf]:               https://www.github.com/oh-my-fish/oh-my-fish
[omf_image]:         https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square

[mit]:               ./LICENSE
[licence_mit_image]: https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square

[ngrok]:             https://ngrok.com/

[author]:            https://gitlab.com/pinage404
[contributors]:      https://gitlab.com/pinage404/omf_pkg_ngrok/graphs/master
